import csv

politics_list = ["obama", "clinton", "hillary", "poll", "election", "bernie", "congress", "law", "republican", "democrat", "president"]
news_list = ["cnn", "nyt", "new york times", "fox", "fox news", "msnbc"]
special_phrases = ["fake news", "russia", "witch hunt", "make america great again", "maga", "collusion", "impeach"]

with open('trumptweets.csv', 'r', encoding="utf-8") as f:
    reader = csv.reader(f)
    tweet_list = list(reader)

#print(tweet_list)
for record in tweet_list:
    tmp = record[3].split()
    caps_counter = 0
    politics_counter = 0 
    news_counter = 0
    special_counter = 0
    for word in tmp:
        if(word.isupper()):
            caps_counter = caps_counter + 1
    for phrase in politics_list:
        if(phrase in record[3].lower()):
            politics_counter = politics_counter + 1
    for phrase in news_list:
        if(phrase in record[3].lower()):
            news_counter = news_counter + 1
    for phrase in special_phrases:
        if(phrase in record[3].lower()):
            special_counter = special_counter + 1
    record[5] = caps_counter
    record[6] = politics_counter
    record[7] = news_counter
    record[8] = special_counter

with open('trumptweetseval.csv', 'w', encoding="utf-8") as f:
    filewriter = csv.writer(f, delimiter=',')
    filewriter.writerow(['id_str','created_at','author','text','is_trump','caps_counter','politics_counter','news_counter','phrase_counter'])
    for record in tweet_list:
        filewriter.writerow(record)
print("done")